# Student Ranklist

Input: Scores of students; n students; m subjects

Each line is m marks -- all are non-zero

The marks are unique across each subject; that is no two students get the same mark in the same subject.

If each mark of a student S is greater than or equal to the corresponding mark of T, then we say S > T

If each mark of a student S is less than the corresponding mark of T, then we say S < T

If some marks of S are greater and some are lesser we say that they are uncomparable, denoted by A # B

Of course if A > B and B > C then A > C

Write a program that reads the input and outputs the smallest number of lines of the form A > B or A # B to represent the final status completely.

Functionalities Used and work distribution

  1.Load data and parse data functions to get the data from the text file and its various fields - Tanya Tripathi

  2.Function to create a ranklist from the data obtained from the file and display the ranklist - Pranjalaa Rai 

 
