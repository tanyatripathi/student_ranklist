SPACE, COMMENT = ' ', '#'

class StudentRankList():
    def __init__(self, filename):
        self.filename = filename
        self.records = self.load_data(self.filename)
    
    def parse(self, line: str) -> tuple[int, int, str, list[int]]:
        fields = line.strip().split()
        marks = [int(f) for f in fields if f.isdigit()]
        fails = len([mark for mark in marks if mark < 40])
        name = SPACE.join(f for f in fields if not f.isdigit())
        return fails, sum(marks), name, marks

    def load_data(self, filename: str) -> list[tuple[int, str]]:
        return [self.parse(line) for line in open(filename) if line[0] != COMMENT]
    
    def make_ranklist(self) -> list[str]:
        ranklist = []
        data = sorted(self.records, key=lambda rec: (rec[0], -rec[1]))
        rank = 0
        prev_total_marks = -1

        for position, rec in enumerate(data, start=1):
            if rec[1] != prev_total_marks:
                rank = position
                prev_total_marks = rec[1]
            ranklist.append(f'{rank:4}{rec[1]:7} {rec[2]:40}{rec[3]}')

        return ranklist
    
    def display_ranklist(self):
        for line in self.make_ranklist():
            print(line)



filename = "studentmarks.txt"
rank_list_obj = StudentRankList(filename)
rank_list_obj.display_ranklist()